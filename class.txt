import Foundation
//Создать класс родитель и 2 класса наследника
class Person {
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }   
}
class Teacher: Person {
    var subject: String
    
    init(name: String, age: Int, subject: String) {
        self.subject = subject
        super.init(name: name, age: age)
    } 
}
class Student: Person {
    var grade: Int
    
    init(name: String, age: Int, grade: Int) {
        self.grade = grade
        super.init(name: name, age: age)
    }
}


// Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)
class House {
    var width: Double
    var height: Double
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("Создан дом площадью \(area) квадратных метров.")
    }
    
    func destroy() {
        print("Дом уничтожен.")
    }
}
let myHouse = House(width: 12.4, height: 5.8)
myHouse.create()    
myHouse.destroy()  
print("--------------------")


//Создайте класс с методами, которые сортируют массив учеников по разным параметрам
class StudentManager {
    var students: [Student]
    
    init(students: [Student]) {
        self.students = students
    }
    
    func sortByName() {
        students.sort { $0.name < $1.name }
    }
    
    func sortByAge() {
        students.sort { $0.age < $1.age }
    }
    
    func sortByGrade() {
        students.sort { $0.grade < $1.grade }
    }
    
    func printStudents() {
        for student in students {
            print("Имя: \(student.name), Возраст: \(student.age), Класс: \(student.grade)")
        }
    }
}

let student1 = Student(name: "Алиса", age: 15, grade: 10)
let student2 = Student(name: "Григорий", age: 16, grade: 11)
let student3 = Student(name: "Миша", age: 14, grade: 9)
let manager = StudentManager(students: [student1, student2, student3])

manager.sortByName()
print("Отсортировано по имени:")
manager.printStudents()
print("--")
manager.sortByAge()
print("\nОтсортировано по возрасту:")
manager.printStudents()
print("--")
manager.sortByGrade()
print("\nОтсортировано по классу:")
manager.printStudents()
print("--------------------")


//Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов
//Рассмотрим на примере класса House и экземпляра myHouse, созданных выше
struct Building {
    var width: Double
    var height: Double
    
    // Структуры также могут иметь методы
    func calculateArea() -> Double {
        return width * height
    }
}

var myBuilding = Building(width: 12.4, height: 5.8)
// Структуры передаются по значению, поэтому при присваивании создается копия
var anotherBuilding = myBuilding
anotherBuilding.width = 10.0
// После изменения ширины одного экземпляра, другой экземпляр остается неизменным
print("В структуре другой экземпляр остается неизменным:")
print("Ширина моего здания: \(myBuilding.width)")
print("Ширина другого здания: \(anotherBuilding.width)")
print("--")
// А классы передаются по ссылке, поэтому переменные ссылаются на один и тот же объект в памяти, теперь попробуем создать еще один объект, ссылающийся на тот же дом
let anotherHouse = myHouse
// Попробуем изменить ширину дома через новый объект
anotherHouse.width = 10.0
print("В классе происходит изменение модели через другую ссылку:")
print("Ширина моего дома: \(myHouse.width)")
print("Ширина другого дома: \(anotherHouse.width)")
print("--------------------")
// Различия между классом и структурой:
// 1. Передача по значению или по ссылке
// Как было рассмотрено выше, структуры передаются по значению, поэтому при присваивании создается копия. 
// При изменении свойства одной структуры это не отражается на другой структуре.
// В случае с классами, они передаются по ссылке, поэтому переменные ссылаются на один и тот же объект в памяти. 
// Если изменить свойство одного объекта, это отразится на всех переменных, которые ссылаются на этот объект.
// 2. Наследование
// Классы поддерживают наследование, что означает, что вы можете создать новый класс на основе существующего класса и добавить или изменить его функциональность. Структуры не поддерживают наследование.




/*
▸ Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт
▸ Сохраняйте комбинации в массив
▸ Если выпала определённая комбинация - выводим соответствующую запись в консоль
▸ Результат в консоли примерно такой: 'У вас бубновый стрит флеш'.
*/

// Enum для типов карт
enum CardRank: String, CaseIterable {
    case two = "2", three = "3", four = "4", five = "5", six = "6", seven = "7", eight = "8", nine = "9", ten = "10"
    case jack = "J", queen = "Q", king = "K", ace = "A"
}

// Enum для мастей
enum CardSuit: String, CaseIterable {
    case hearts = "♥", diamonds = "♦", clubs = "♣", spades = "♠"
}

// Enum для различных комбинаций в покере
enum PokerHand: String {
    case highCard = "Старшая карта"
    case onePair = "Пара"
    case twoPair = "Две пары"
    case threeOfAKind = "Тройка"
    case straight = "Стрит"
    case flush = "Флеш"
    case fullHouse = "Фулл хаус"
    case fourOfAKind = "Каре"
    case straightFlush = "Стрит флеш"
}

// Класс для проверки комбинаций в покере
class PokerHandChecker {
    // Функция, которая подсчитывает количество карт каждого ранга
    private func countRanks(_ hand: [(rank: CardRank, suit: CardSuit)]) -> [CardRank: Int] {
        var rankCounts = [CardRank: Int]()
        for card in hand {
            rankCounts[card.rank, default: 0] += 1
        }
        return rankCounts
    }

    // Функция, которая определяет, есть ли на руке одна пара
    private func hasOnePair(_ ranks: [CardRank: Int]) -> Bool {
        return ranks.values.contains(2)
    }

    // Функция, которая определяет, есть ли на руке две пары
    private func hasTwoPair(_ ranks: [CardRank: Int]) -> Bool {
        return ranks.values.filter { $0 == 2 }.count == 2
    }

    // Функция, которая определяет, есть ли на руке тройка
    private func hasThreeOfAKind(_ ranks: [CardRank: Int]) -> Bool {
        return ranks.values.contains(3)
    }

    // Функция, которая определяет, есть ли на руке стрит
    private func hasStraight(_ hand: [(rank: CardRank, suit: CardSuit)]) -> Bool {
        let sortedRanks = hand.map { $0.rank.rawValue }.sorted()
        for i in 0..<sortedRanks.count - 1 {
            if Int(sortedRanks[i + 1])! - Int(sortedRanks[i])! != 1 {
                return false
            }
        }
        return true
    }



    // Функция, которая определяет, есть ли на руке флеш
    private func hasFlush(_ hand: [(rank: CardRank, suit: CardSuit)]) -> Bool {
        let suit = hand.first!.suit
        return hand.allSatisfy { $0.suit == suit }
    }
    
    // Функция, которая определяет, есть ли на руке стрит флеш
    private func hasStraightFlush(_ hand: [(rank: CardRank, suit: CardSuit)]) -> Bool {
        return hasFlush(hand) && hasStraight(hand)
    }

    // Функция, которая определяет, есть ли на руке фулл хаус
    private func hasFullHouse(_ ranks: [CardRank: Int]) -> Bool {
        return ranks.values.contains(3) && ranks.values.contains(2)
    }

    // Функция, которая определяет, есть ли на руке каре
    private func hasFourOfAKind(_ ranks: [CardRank: Int]) -> Bool {
        return ranks.values.contains(4)
    }

    // Функция для проверки комбинации в покере
    func checkPokerHand(_ cards: [(rank: CardRank, suit: CardSuit)]) -> PokerHand {
        let ranks = countRanks(cards)
        
        if hasStraightFlush(cards) {
            return .straightFlush
        } else if hasFourOfAKind(ranks) {
            return .fourOfAKind
        } else if hasFullHouse(ranks) {
            return .fullHouse
        } else if hasFlush(cards) {
            return .flush
        } else if hasStraight(cards) {
            return .straight
        } else if hasThreeOfAKind(ranks) {
            return .threeOfAKind
        } else if hasTwoPair(ranks) {
            return .twoPair
        } else if hasOnePair(ranks) {
            return .onePair
        } else {
            return .highCard
        }
    }
}

// Генерация случайной руки из 5 карт
func generateRandomHand() -> [(rank: CardRank, suit: CardSuit)] {
    var hand = [(rank: CardRank, suit: CardSuit)]()
    while hand.count < 5 {
        let rank = CardRank.allCases.randomElement()!
        let suit = CardSuit.allCases.randomElement()!
        if !hand.contains(where: { $0.rank == rank && $0.suit == suit }) {
            hand.append((rank, suit))
        }
    }
    return hand
}


for layoutNumber in 1...2 {
    // Выводим номер раскладки
    print("Раскладка №\(layoutNumber):")

    // Выводим набор карт на руке
    print("На руку легли карты:")
    let hand = generateRandomHand()
    for card in hand {
        print("\(card.rank.rawValue)\(card.suit.rawValue)", terminator: " ")
    }
    print()

    // Проверяем комбинацию и выводим результат
    let pokerHandChecker = PokerHandChecker()
    let handType = pokerHandChecker.checkPokerHand(hand)
    print("У вас \(handType.rawValue.lowercased())\n")
    print("--")
}

